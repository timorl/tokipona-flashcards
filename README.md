# toki pona flashcards

Flashcards for learning toki pona words in LaTeX.

## Compiling

First run `make_images.sh`, requires imagemagick. Then however you compile your LaTeX files should work.

## Licence

The images are taken from Wikimedia Commons [here](https://commons.wikimedia.org/wiki/Category:SVGs_of_sitelen_pona_vectorized_by_LiliCharlie) and they are CC0.

I guess whatever I wrote can also be CC0, have fun.
