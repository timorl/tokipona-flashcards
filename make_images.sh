#!/bin/sh

for svg_file in *.svg
do
	filename=${svg_file%.*}
	convert ${filename}.svg ${filename}.png
done
